export const LOCAL_DOMAIN = Deno.env.get('LOCAL_DOMAIN') || 'http://localhost:8000';
export const NOSTR_RELAY = Deno.env.get('NOSTR_RELAY') || 'wss://relay.mostr.pub';

export const getSecretKey = () => Deno.env.get('SECRET_KEY');

if (!getSecretKey()) {
  throw 'SECRET_KEY is not set! Please generate one with `openssl rand -base64 48`';
}

const deduplicate = (items: string[]): string[] => Array.from(new Set(items));
const parseRelays = (relays: string | undefined) => deduplicate((relays || '').split(',').filter(Boolean));

export const publishRelays = deduplicate([NOSTR_RELAY, ...parseRelays(Deno.env.get('RELAY_PUBLISHERS'))]);
export const poolRelays = parseRelays(Deno.env.get('RELAY_POOL'));
export const allRelays = deduplicate([...publishRelays, ...poolRelays]);

export const DB_PATH = Deno.env.get('DB_PATH') || 'db.sqlite3';
