import { RelayPool } from '@/deps.ts';

import { publishRelays } from '../config.ts';
import { nostrDate } from '../utils/parse.ts';

import handleEvent from './handler.ts';

import type { Event } from './event.ts';

const pool = new RelayPool(publishRelays);

pool.subscribe(
  [{ kinds: [0, 1, 3, 5, 6, 7, 9735], since: nostrDate() }],
  publishRelays,
  (event: Event) => {
    console.log(`Event<${event.kind}>`, event.id);
    handleEvent(event);
  },
  undefined,
  undefined,
);

export { pool };
