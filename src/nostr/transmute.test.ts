import { assertEquals } from '@/deps.ts';

import actor from '../../fixtures/activitypub/actor-pleroma.json' assert { type: 'json' };
import actorWithEmoji from '../../fixtures/activitypub/actor-with-emoji.json' assert { type: 'json' };
import note from '../../fixtures/activitypub/note-fedibird.json' assert { type: 'json' };
import noteWithLinebreaks from '../../fixtures/activitypub/note-with-linebreaks.json' assert { type: 'json' };
import noteSensitive from '../../fixtures/activitypub/note-sensitive.json' assert { type: 'json' };
import noteWithHashtag from '../../fixtures/activitypub/note-with-hashtag.json' assert { type: 'json' };
import noteWithHashtags from '../../fixtures/activitypub/note-with-hashtags.json' assert { type: 'json' };
import noteWithMarkdown from '../../fixtures/activitypub/note-with-markdown.json' assert { type: 'json' };
import noteWithEmoji from '../../fixtures/activitypub/note-with-emoji.json' assert { type: 'json' };
import quotePost from '../../fixtures/activitypub/quote-post.json' assert { type: 'json' };

import { toEvent0, toEvent1, toEvent10002, toEvent6 } from './transmute.ts';

import type { Actor, Note } from '../activitypub/schema.ts';
import { cipher } from '../db.ts';

Deno.test('toEvent0', async () => {
  const event = await toEvent0(actor as Actor);

  const expected: typeof event = {
    kind: 0,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content:
      '{"name":"Alex Gleason","about":"I create Fediverse software that empowers people online.\\n\\nI\'m vegan btw\\n\\nNote: If you have a question for me, please tag me publicly. This gives the opportunity for others to chime in, and bystanders to learn.","picture":"https://media.gleasonator.com/9059767a327659dad5d43a2a7b3f048b7008cea76e58dc6ba79b3838feaf25e5.png","banner":"https://media.gleasonator.com/e5f6e0e380536780efa774e8d3c8a5a040e3f9f99dbb48910b261c32872ee3a3.gif","nip05":"alex_at_gleasonator.com@localhost:8000","lud16":"alexgleason@getalby.com"}',
    tags: [
      ['proxy', 'https://gleasonator.com/users/alex', 'activitypub'],
    ],
    created_at: event.created_at,
    id: event.id,
    sig: event.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent0 with custom emoji', async () => {
  const event = await toEvent0(actorWithEmoji as Actor);

  const expected: typeof event = {
    kind: 0,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content:
      '{"name":"Alex Gleason :soapbox:","about":"I create Fediverse software that empowers people online.\\n\\nI\'m vegan btw.\\n\\nNote: If you have a question for me, please tag me publicly. This gives the opportunity for others to chime in, and bystanders to learn.","picture":"https://media.gleasonator.com/9059767a327659dad5d43a2a7b3f048b7008cea76e58dc6ba79b3838feaf25e5.png","banner":"https://media.gleasonator.com/e5f6e0e380536780efa774e8d3c8a5a040e3f9f99dbb48910b261c32872ee3a3.gif","nip05":"alex_at_gleasonator.com@localhost:8000","lud16":"alex@alexgleason.me"}',
    tags: [
      ['emoji', 'soapbox', 'https://gleasonator.com/emoji/Gleasonator/soapbox.png'],
      ['proxy', 'https://gleasonator.com/users/alex', 'activitypub'],
    ],
    created_at: event.created_at,
    id: event.id,
    sig: event.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent1', async () => {
  const event = await toEvent1(note as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: 'c9be7cf8727d140311a303b08a10cce931569c0b8ed292342edea2b55b6a5e4a',
    content: 'nostr:npub19razeqkd7c9gcp4efujg9ew7n7fwtngrkqc5ph49u8pac9eahaqs6w923v hi',
    tags: [
      [
        'p',
        '28fa2c82cdf60a8c06b94f2482e5de9f92e5cd03b03140dea5e1c3dc173dbf41',
        'wss://relay.mostr.pub',
      ],
      [
        'e',
        '9e4ad2472288b802812cd2d4ba255788fd6c339f97510c2c81f1204511bcc4a8',
        'wss://relay.mostr.pub',
        'reply',
      ],
      [
        'proxy',
        'https://fedibird.com/users/alex/statuses/109900001983484618',
        'activitypub',
      ],
    ],
    created_at: 1676940948,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent1 converts <br> to newlines', async () => {
  const event = await toEvent1(noteWithLinebreaks as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content:
      'The Mostr bridge is now being published on 3 relays:\n\n- wss://relay.mostr.pub (run by me)\n- wss://nostr.poster.place nostr:npub1wym70akxsmq2uk2m7pzn7ygv8txax075s7slkf34f87rtvm2aefq9hf022\n\- wss://relay.shitforce.one nostr:npub1fgeakdgrs9gdawvk037025l4zx2esx5kvfnfwxelq4kdllk9smmqged4m7\n\nYou can connect to any or all of them to ensure fediverse shitposts make it to your nostrsphere.',
    tags: [
      [
        'p',
        '7137e7f6c686c0ae595bf0453f110c3acdd33fd487a1fb263549fc35b36aee52',
        'wss://relay.mostr.pub',
      ],
      [
        'p',
        '4a33db35038150deb9967c7cf553f51195981a966266971b3f056cdffec586f6',
        'wss://relay.mostr.pub',
      ],
      [
        'proxy',
        'https://gleasonator.com/objects/043511be-03d7-408e-a981-09717a435f01',
        'activitypub',
      ],
    ],
    created_at: 1677648033,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event!.content, expected.content);
  assertEquals(event, expected);
});

Deno.test('toEvent1 handles sensitive content', async () => {
  const event = await toEvent1(noteSensitive as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content: '👻',
    tags: [
      ['content-warning', 'warning do not click'],
      [
        'proxy',
        'https://gleasonator.com/objects/40c2b180-4932-49f3-91cd-c07f8180d6c7',
        'activitypub',
      ],
    ],
    created_at: 1678841850,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent1 with hashtag', async () => {
  const event = await toEvent1(noteWithHashtag as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content: 'Hello #Nostrica',
    tags: [
      ['t', 'nostrica'],
      [
        'proxy',
        'https://gleasonator.com/objects/2f6938c0-7600-483b-bb71-eca36454bcb3',
        'activitypub',
      ],
    ],
    created_at: 1679238062,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent1 with hashtags and mentions', async () => {
  const event = await toEvent1(noteWithHashtags as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '84abba3841f5a805d0b1211dea4daf4c3dddf5f98a0765e16c1bdff0d4fc5708',
    content:
      'Wer jetzt von #LineageOS (zu Recht) enttäuscht ist, der kann sich mal #iodéOS anschauen. Die schneiden bezüglich Datenschutzfreundlichkeit bisher am besten ab. 👇 \n\nhttps://www.kuketz-blog.de/iodeos-datenschutzfreundlich-aber-abstriche-bei-der-sicherheit-custom-roms-teil3/\n\n#android #customrom #datenschutz #privacy',
    tags: [
      ['t', 'privacy'],
      ['t', 'datenschutz'],
      ['t', 'customrom'],
      ['t', 'android'],
      ['t', 'IodeOS'],
      ['t', 'lineageos'],
      [
        'proxy',
        'https://social.tchncs.de/users/kuketzblog/statuses/110258145039516469',
        'activitypub',
      ],
    ],
    created_at: 1682405777,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent1 with quote post', async () => {
  cipher.add({
    apId: 'https://metalhead.club/users/seanmunger/statuses/110251045576089544',
    nostrId: 'c6708ed42ed42bad580a7b5105630c8adff6e9bc87865b077ccd09bcd8eaa846',
  });

  const event = await toEvent1(quotePost as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content: 'I invented the internet, guys.\n\nnostr:note1cecga4pw6s466kq20dgs2ccv3t0ld6dus7r9kpmue5ymek824prqs9hcre',
    tags: [
      [
        'p',
        'c6708ed42ed42bad580a7b5105630c8adff6e9bc87865b077ccd09bcd8eaa846',
        'wss://relay.mostr.pub',
      ],
      [
        'proxy',
        'https://gleasonator.com/objects/d45f6faa-00ac-4358-a8ba-4489f075b55a',
        'activitypub',
      ],
    ],
    created_at: 1682347409,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent1 with markdown', async () => {
  const event = await toEvent1(noteWithMarkdown as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content:
      'Hello world\n\nfunction cleanContent(content: string) {\n  return content\n    .replace(/\\n{0,2}#\\[\\d\\]$/, \'\')\n    .replaceAll(\'\\n\', \'<br />\');\n}\n\none\ntwo\nthree\n\nDon’t tell me “looks good or my end” or “hurr durr looks like there’s a problem with it” yeah that’s what I’m testing retard I have eyes\n\nfour\nfive\nsix',
    tags: [
      [
        'proxy',
        'https://gleasonator.com/objects/08bc9e43-a9b9-4daa-88ea-23d983ec14a0',
        'activitypub',
      ],
    ],
    created_at: 1679250022,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event!.content, expected.content);
  assertEquals(event, expected);
});

Deno.test('toEvent1 with custom emojis', async () => {
  const event = await toEvent1(noteWithEmoji as Note);

  const expected: typeof event = {
    kind: 1,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content: 'hello :gleasonator:',
    tags: [
      ['emoji', 'gleasonator', 'https://gleasonator.com/emoji/Gleasonator/gleasonator.png'],
      [
        'proxy',
        'https://gleasonator.com/objects/2539897e-0918-43ea-9bc5-6cf4ce1ac35f',
        'activitypub',
      ],
    ],
    created_at: 1682632922,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent6', async () => {
  cipher.add({ apId: 'https://gleasonator.com/objects/1', nostrId: 'gg' });

  const event = await toEvent6({
    id: 'https://mostr.pub/1',
    type: 'Announce',
    actor: 'https://mostr.pub/@alex',
    to: [],
    cc: [],
    published: '2023-01-01T00:00:00Z',
    object: 'https://gleasonator.com/objects/1',
  });

  const expected: typeof event = {
    kind: 6,
    pubkey: '1365217f6c7e959efba17ce7a8a03f6d8a53a8a9cf05404bb0821565ddcba0e5',
    content: '',
    tags: [
      ['e', 'gg', 'wss://relay.mostr.pub'],
      ['proxy', 'https://gleasonator.com/objects/1', 'activitypub'],
    ],
    created_at: event!.created_at,
    id: event!.id,
    sig: event!.sig,
  };

  assertEquals(event, expected);
});

Deno.test('toEvent10002', async () => {
  const event = await toEvent10002(actor as Actor);

  const expected: typeof event = {
    kind: 10002,
    pubkey: '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b',
    content: '',
    tags: [
      ['r', 'wss://relay.mostr.pub'],
      ['proxy', 'https://gleasonator.com/users/alex', 'activitypub'],
    ],
    created_at: event.created_at,
    id: event.id,
    sig: event.sig,
  };

  assertEquals(event, expected);
});
