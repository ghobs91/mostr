import { pool } from './relay.ts';
import { signEvent } from './sign.ts';
import { toEvent0, toEvent1, toEvent10002 } from './transmute.ts';

import { fetchObject } from '../activitypub/client.ts';
import { Actor, Note } from '../activitypub/schema.ts';
import { isActor } from '../activitypub/utils.ts';
import { allRelays, publishRelays } from '../config.ts';
import { cipher, followsDB } from '../db.ts';
import { nostrDate } from '../utils/parse.ts';

import type { EventTemplate, SignedEvent } from './event.ts';

/** Publish an event to the Nostr relay. */
function publish(event: SignedEvent, relays = publishRelays): void {
  console.log('Publishing event', event);
  try {
    pool.publish(event, relays);
  } catch (e) {
    console.error(e);
  }
}

async function fetchAndPublishActor(actorId: string): Promise<void> {
  if (!cipher.getNostrId(actorId)) {
    const actor = await fetchObject(actorId);

    if (actor && isActor(actor)) {
      return publishActor(actor);
    }
  }
}

function maybePublishActor(actor: Actor) {
  if (!cipher.getNostrId(actor.id)) {
    return publishActor(actor);
  }
}

async function publishActor(actor: Actor) {
  const [event0, event10002] = await Promise.all([
    toEvent0(actor),
    toEvent10002(actor),
  ]);

  cipher.add({ apId: actor.id, nostrId: event0.pubkey });
  publish(event0);
  publish(event10002, allRelays);
}

async function fetchAndPublishObject(objectId: string, i = 0): Promise<void> {
  if (i > 100) return; // Prevent reply-bomb.

  if (!cipher.getNostrId(objectId)) {
    const object = await fetchObject(objectId);

    if (object?.type === 'Note') {
      if (object.inReplyTo) {
        await fetchAndPublishObject(object.id, i + 1);
      }

      return publishNote(object);
    }
  }
}

async function publishNote(note: Note) {
  await fetchAndPublishActor(note.attributedTo);

  const event = await toEvent1(note);

  if (event && event.id) {
    cipher.add({ apId: note.id, nostrId: event.id });
    publish(event);
  }
}

async function publishFollows(actorId: string) {
  const follows = followsDB.getFollows(actorId);

  const event: EventTemplate<3> = {
    kind: 3,
    content: '',
    tags: follows.map((p) => ['p', p]),
    created_at: nostrDate(),
  };

  publish(await signEvent(event, actorId));
}

export { fetchAndPublishActor, fetchAndPublishObject, maybePublishActor, publish, publishFollows, publishNote };
