import { DOMParser, nip19, z } from '@/deps.ts';

import { getActorAcct, isActorId } from '../activitypub/utils.ts';
import { LOCAL_DOMAIN, NOSTR_RELAY, publishRelays } from '../config.ts';
import { cipher } from '../db.ts';
import { addHtmlBreaks, nostrDate, toNostrDate } from '../utils/parse.ts';

import { MetaContent } from './schema.ts';
import { signEvent } from './sign.ts';
import { parseContent, processMentions, stripCompatFeatures } from './transmute/content.ts';
import { toPubkey } from './utils.ts';

import type { EventTemplate, SignedEvent } from './event.ts';
import type {
  Actor,
  Announce,
  Delete,
  Emoji,
  EmojiReact,
  Hashtag,
  Like,
  Mention,
  Note,
} from '../activitypub/schema.ts';

function sanitizeHtml(html: string): string {
  html = addHtmlBreaks(html);
  const doc = new DOMParser().parseFromString(html, 'text/html');
  return doc?.textContent || '';
}

function getNip05(actor: Actor): string {
  const { host } = new URL(LOCAL_DOMAIN);
  const acct = getActorAcct(actor);
  return `${acct.replace('@', '_at_')}@${host}`;
}

/** Convert ActivityPub ID into "p" tag. */
async function apIdToPTag(apId: string): Promise<string[]> {
  const pubkey = await toPubkey(apId);
  return ['p', pubkey, NOSTR_RELAY];
}

function toEvent0(actor: Actor): Promise<SignedEvent<0>> {
  const lud16 = actor.attachment.find((pv) => pv.name.toLowerCase() === 'lud16')?.value;

  const content: MetaContent = {
    name: actor.name,
    about: sanitizeHtml(actor.summary),
    picture: actor.icon?.url,
    banner: actor.image?.url,
    nip05: getNip05(actor),
    lud16: z.string().email().safeParse(lud16).success ? lud16 : undefined,
  };

  const emojis = actor.tag
    .filter((e): e is Emoji => e.type === 'Emoji')
    .map((e) => ['emoji', e.name.replace(/(^:|:$)/g, ''), e.icon.url]);

  const event: EventTemplate<0> = {
    kind: 0,
    content: JSON.stringify(content),
    tags: [
      ...emojis,
      ['proxy', actor.id, 'activitypub'],
    ],
    created_at: nostrDate(),
  };

  return signEvent(event, actor.id);
}

async function toEvent1(note: Note): Promise<SignedEvent<1> | undefined> {
  const replyId = getNostrId(note.inReplyTo);
  // If the note is a reply and we don't have its parent, die immediately!
  if (note.inReplyTo && !replyId) return;

  const doc = parseContent(addHtmlBreaks(note.content));
  if (doc) stripCompatFeatures(doc);
  const inlineMentions = doc ? await processMentions(doc) : [];

  const mentions = note.tag
    .filter((t): t is Mention => t.type === 'Mention')
    .map((t) => t.href);

  const hashtags = note.tag
    .filter((t): t is Hashtag => t.type === 'Hashtag')
    .map((t) => ['t', t.name.replaceAll('#', '')]);

  const emojis = note.tag
    .filter((e): e is Emoji => e.type === 'Emoji')
    .map((e) => ['emoji', e.name.replace(/(^:|:$)/g, ''), e.icon.url]);

  const tags = await Promise.all(
    [
      ...new Set([
        ...inlineMentions,
        ...note.to,
        ...note.cc,
        ...mentions,
      ]),
    ]
      .filter(isActorId)
      .map(apIdToPTag),
  );

  const event: EventTemplate<1> = {
    kind: 1,
    content: doc?.textContent || '',
    created_at: toNostrDate(note.published),
    tags,
  };

  if (replyId) {
    event.tags.push([
      'e',
      replyId,
      NOSTR_RELAY,
      'reply',
    ]);
  }

  const quoteUrl = getNostrId(note.quoteUrl);
  if (quoteUrl) {
    event.content += '\n\nnostr:' + nip19.noteEncode(quoteUrl);
  } else if (note.quoteUrl) {
    return;
  }

  event.tags.push(...hashtags, ...emojis);

  if (note.sensitive) {
    event.tags.push(['content-warning', note.summary || ''].filter(Boolean));
  }

  event.tags.push(['proxy', note.id, 'activitypub']);

  note.attachment?.forEach((a) => {
    event.content += '\n\n' + a.url;
  });

  return signEvent(event, note.attributedTo);
}

function toEvent5(activity: Delete): Promise<SignedEvent<5>> | undefined {
  const targetId = getNostrId(activity.object);
  if (!targetId) return;

  const event: EventTemplate<5> = {
    kind: 5,
    content: '',
    tags: [
      ['e', targetId],
      ['proxy', activity.id, 'activitypub'],
    ],
    created_at: nostrDate(),
  };

  return signEvent(event, activity.actor);
}

function toEvent6(activity: Announce): Promise<SignedEvent<6>> | undefined {
  const objectId = typeof activity.object === 'string' ? activity.object : activity.object.id;
  const repostId = getNostrId(objectId);
  if (!repostId) return;

  const event: EventTemplate<6> = {
    kind: 6,
    content: '',
    tags: [
      ['e', repostId, NOSTR_RELAY],
      ['proxy', objectId, 'activitypub'],
    ],
    created_at: toNostrDate(activity.published),
  };

  return signEvent(event, activity.actor);
}

function likeToEvent7(activity: Like): Promise<SignedEvent<7>> | undefined {
  const targetId = getNostrId(activity.object);
  if (!targetId) return;

  const event: EventTemplate<7> = {
    kind: 7,
    content: '+',
    tags: [
      ['e', targetId],
      ['proxy', activity.id, 'activitypub'],
    ],
    created_at: nostrDate(),
  };

  return signEvent(event, activity.actor);
}

function emojiReactToEvent7(activity: EmojiReact): Promise<SignedEvent<7>> | undefined {
  const targetId = getNostrId(activity.object);
  if (!targetId) return;

  const event: EventTemplate<7> = {
    kind: 7,
    content: activity.content,
    tags: [
      ['e', targetId],
      ['proxy', activity.id, 'activitypub'],
    ],
    created_at: nostrDate(),
  };

  return signEvent(event, activity.actor);
}

function toEvent7(activity: Like | EmojiReact): Promise<SignedEvent<7>> | undefined {
  switch (activity.type) {
    case 'Like':
      return likeToEvent7(activity);
    case 'EmojiReact':
      return emojiReactToEvent7(activity);
  }
}

function toEvent10002(actor: Actor): Promise<SignedEvent<10002>> {
  const event: EventTemplate<10002> = {
    kind: 10002,
    content: '',
    tags: [
      ...publishRelays.map((r) => ['r', r]),
      ['proxy', actor.id, 'activitypub'],
    ],
    created_at: nostrDate(),
  };

  return signEvent(event, actor.id);
}

function getNostrId(apId: string | undefined): string | undefined {
  if (apId?.startsWith(`${LOCAL_DOMAIN}/objects/`)) {
    return new URL(apId).pathname.split('/')[2];
  } else if (apId) {
    return cipher.getNostrId(apId);
  }
}

export { toEvent0, toEvent1, toEvent10002, toEvent5, toEvent6, toEvent7 };
