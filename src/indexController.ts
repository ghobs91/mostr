import { LOCAL_DOMAIN, poolRelays, publishRelays } from './config.ts';

import type { Context } from './deps.ts';

/** Build a list of Markdown items. */
const mdList = (values: string[]): string => values.map((value) => `- ${value}`).join('\n');

/** Indent multi-line text by `size` spaces. */
const indent = (text: string, size: number): string =>
  text.split('\n')
    .map((line) => `${' '.repeat(size)}${line}`)
    .join('\n');

function indexController(c: Context) {
  return c.text(`Mostr: a bridge between Nostr and the Fediverse

  Discover users:
      ${LOCAL_DOMAIN}/users/<pubkey>

  Discover events:
      ${LOCAL_DOMAIN}/objects/<eventid>

  NIP-05:
      ${LOCAL_DOMAIN}/.well-known/nostr.json?name=alex_at_gleasonator.com

  Relays (read/write):
${indent(mdList(publishRelays), 2)}

  Relays (read-only):
${indent(mdList(poolRelays), 2)}

  Support us!

  - BTC: bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n
  - https://soapbox.pub/donate

  Source: https://gitlab.com/soapbox-pub/mostr
  `);
}

export { indexController };
