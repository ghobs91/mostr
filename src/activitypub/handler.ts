import { allRelays } from '../config.ts';
import { cipher, followsDB } from '../db.ts';
import { fetchAndPublishActor, fetchAndPublishObject, publish, publishFollows } from '../nostr/publisher.ts';
import { toEvent0, toEvent1, toEvent10002, toEvent5, toEvent6, toEvent7 } from '../nostr/transmute.ts';
import { apIdToPubkey } from '../utils/parse.ts';

import { buildAccept } from './builder.ts';
import { AP_PUBLIC_URI } from './constants.ts';
import { federate } from './federation.ts';

import type { Activity, Announce, CreateNote, Delete, EmojiReact, Follow, Like, Note, Update } from './schema.ts';

/** Perform side-effects for activity through pipeline. */
function handleActivity(activity: Activity) {
  try {
    console.log(`Activity<${activity.type}>`, activity.id);

    // Publish the event's actor so it's available in Nostr.
    if (activity.type !== 'Update') {
      fetchAndPublishActor(activity.actor);
    }

    switch (activity.type) {
      case 'Follow':
        return handleFollow(activity);
      case 'Create':
        switch (activity.object.type) {
          case 'Note':
            return handleCreateNote(activity);
        }
        return;
      case 'Announce':
        return handleAnnounce(activity);
      case 'Update':
        return handleUpdate(activity);
      case 'Like':
        return handleLike(activity);
      case 'EmojiReact':
        return handleEmojiReact(activity);
      case 'Delete':
        return handleDelete(activity);
    }
  } catch (e) {
    console.error(e);
  }
}

function handleFollow(activity: Follow) {
  followsDB.addFollow(activity.actor, apIdToPubkey(activity.object));
  publishFollows(activity.actor);
  return federate(buildAccept(activity));
}

async function handleCreateNote({ object }: CreateNote) {
  if (!isObjectPublic(object)) return;

  if (object.quoteUrl) {
    await fetchAndPublishObject(object.quoteUrl);
  }

  if (object.inReplyTo) {
    await fetchAndPublishObject(object.inReplyTo);
  }

  const event = await toEvent1(object);

  if (event) {
    cipher.add({ apId: object.id, nostrId: event.id! });
    publish(event);
  }
}

async function handleAnnounce(activity: Announce) {
  if (!isObjectPublic(activity)) return;

  const objectId = typeof activity.object === 'string' ? activity.object : activity.object.id;
  await fetchAndPublishObject(objectId);

  const event = await toEvent6(activity);
  if (event) {
    publish(event);
  }
}

async function handleUpdate(activity: Update): Promise<void> {
  const [event0, event10002] = await Promise.all([
    toEvent0(activity.object),
    toEvent10002(activity.object),
  ]);

  publish(event0);
  publish(event10002, allRelays);
}

async function handleLike(activity: Like): Promise<void> {
  if (!isObjectPublic(activity)) return;

  const event = await toEvent7(activity);

  if (event) {
    publish(event);
  }
}

async function handleEmojiReact(activity: EmojiReact): Promise<void> {
  if (!isObjectPublic(activity)) return;

  const event = await toEvent7(activity);

  if (event) {
    publish(event);
  }
}

async function handleDelete(activity: Delete): Promise<void> {
  const event = await toEvent5(activity);

  if (event) {
    publish(event, allRelays);
  }
}

/** Whether the Object is publicly available. */
function isObjectPublic(object: Activity | Note): boolean {
  const recipients = [...object.to, ...object.cc];
  return recipients.some((r) => r === AP_PUBLIC_URI);
}

export default handleActivity;
