import { cipher } from '../../db.ts';
import { fetchEvent } from '../../nostr/client.ts';
import { isProxyEvent } from '../../nostr/tags.ts';
import { toNote } from '../transmute.ts';

import { activityJson } from './utils.ts';

import type { Context } from '../../deps.ts';
import type { Event } from '../../nostr/event.ts';

async function objectController(c: Context<'id'>) {
  const id = c.req.param('id');

  if (cipher.getApId(id)) {
    return c.json({ error: 'Not found' }, 404);
  }

  const event = await fetchEvent(id);

  if (event?.kind === 1 && !isProxyEvent(event)) {
    const object = await toNote(event as Event<1>);
    return activityJson(c, object);
  } else {
    return c.json({ error: 'Not found' }, 404);
  }
}

export { objectController };
