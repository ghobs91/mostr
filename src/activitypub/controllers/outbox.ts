import { url } from '../../utils/parse.ts';

import { activityJson } from './utils.ts';

import type { Context } from '../../deps.ts';

function outboxController(c: Context<'pubkey'>) {
  const pubkey = c.req.param('pubkey');

  return activityJson(c, {
    id: url(`/${pubkey}/outbox`),
    type: 'OrderedCollection',
    totalItems: 0,
    orderedItems: [],
  });
}

export { outboxController };
