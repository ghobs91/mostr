import { followsDB } from '../../db.ts';
import { url } from '../../utils/parse.ts';

import { activityJson } from './utils.ts';

import type { Context } from '../../deps.ts';

function followersController(c: Context<'pubkey'>) {
  const pubkey = c.req.param('pubkey');
  const items = followsDB.getFollowers(pubkey);

  return activityJson(c, {
    id: url(`/${pubkey}/followers`),
    type: 'OrderedCollection',
    totalItems: items.length,
    first: {
      id: url(`/${pubkey}/followers?page=1`),
      type: 'OrderedCollectionPage',
      orderedItems: items,
      partOf: url(`/${pubkey}/followers`),
      totalItems: items.length,
    },
  });
}

export { followersController };
