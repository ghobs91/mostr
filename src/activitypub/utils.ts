import { LOCAL_DOMAIN } from '@/config.ts';
import { AP_PUBLIC_URI } from '@/activitypub/constants.ts';

import type { Actor, Object } from './schema.ts';

type Context = (string | Record<string, string | Record<string, string>>)[];

function maybeAddContext<T>(object: T): T & { '@context': Context } {
  return {
    '@context': [
      'https://www.w3.org/ns/activitystreams',
      {
        mostr: 'http://mostr.pub/ns#',
        Zap: 'mostr:Zap',
        xsd: 'http://www.w3.org/2001/XMLSchema#',
        fep: 'https://w3id.org/fep/',
        proxyOf: {
          '@id': 'fep:fffd/proxyOf',
          '@type': '@id',
          '@container': '@set',
        },
        protocol: {
          '@id': 'fep:fffd/protocol',
          '@type': '@id',
        },
        proxied: {
          '@id': 'fep:fffd/proxied',
          '@type': 'xsd:string',
        },
        authoritative: {
          '@id': 'fep:fffd/authoritative',
          '@type': 'xsd:boolean',
        },
      },
    ],
    ...object,
  };
}

function isActor(object: Object): object is Actor {
  return ['Person', 'Application', 'Group', 'Organization', 'Service'].includes(object.type);
}

const isActorId = (apId: string) =>
  apId !== AP_PUBLIC_URI &&
  !apId.endsWith('/followers') &&
  !apId.endsWith('/following');

const isLocalId = (apId: string) => apId.startsWith(LOCAL_DOMAIN);

function getActorAcct(actor: Actor): string {
  const { host } = new URL(actor.id);
  return `${actor.preferredUsername}@${host}`;
}

export { getActorAcct, isActor, isActorId, isLocalId, maybeAddContext };
