import { safeFetch } from '@/deps.ts';

import { fetchObject } from '../activitypub/client.ts';
import { isActor } from '../activitypub/utils.ts';
import ExpiringCache from '../expiring-cache.ts';

import { Webfinger, webfingerSchema } from './webfingerSchema.ts';

import type { Actor } from '../activitypub/schema.ts';

const cache = new ExpiringCache(await caches.open('webfinger'));

/** Resolve an ActivityPub Actor from a Webfinger acct. */
async function finger(acct: string): Promise<Actor | undefined> {
  try {
    const url = getWebfingerURL(acct);
    const cached = await cache.match(url);

    if (cached) {
      const actorId = findActorId(webfingerSchema.parse(await cached.json()));
      const actor = actorId ? await fetchObject(actorId) : undefined;
      return (actor && isActor(actor)) ? actor : undefined;
    } else {
      const response = await safeFetch(url, {
        headers: {
          accept: 'application/json',
        },
      });

      if (response.ok) {
        cache.putExpiring(url, response.clone(), 60 * 60 * 12);
      } else {
        cache.putExpiring(url, response.clone(), 60 * 30);
      }

      const actorId = findActorId(webfingerSchema.parse(await response.json()));
      const actor = actorId ? await fetchObject(actorId) : undefined;
      return (actor && isActor(actor)) ? actor : undefined;
    }
  } catch (_e) {
    return;
  }
}

function findActorId(wf: Webfinger): string | undefined {
  const link = wf.links.find((l) =>
    l.rel === 'self' &&
    [
      'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
      'application/activity+json',
    ].includes(l.type!)
  );
  return link ? link.href : undefined;
}

function getWebfingerURL(acct: string): string {
  const host = acct.split('@')[1];
  if (host) {
    return `https://${host}/.well-known/webfinger?resource=acct:${acct}`;
  } else {
    throw `Webfinger: invalid acct ${acct}`;
  }
}

export { finger };
